package network;

/**
 * Created by kucherenko on 18.04.15.
 */
public class Main {

    public static void main(String[] args) {
        test();
    }

    public static void test() {

        Network network = new Network();
        network = new Trainer(network).getTrainedNetwork();

        for(double i = 0.0; i <= 1.0; i += 0.01) {
            network.setInput(i);
            System.out.println(network.getOutput()[0]);
        }

       /* final double [] x = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};

        for(double value: x) {
            network.setInput(value);
            System.out.println(network.getOutput()[0]);
        }*/


    }


}
