package network;

import layer.InnerLayer;
import layer.Layer;
import layer.OutputLayer;

/**
 * Created by kucherenko on 18.04.15.
 */
public class Network {

    public static final int INNER_NEURON_COUNT = 3;
    public static final int INNER_NEURON_INPUT_COUNT = 1;

    public static final  InnerLayer innerLayer = new InnerLayer(INNER_NEURON_COUNT, INNER_NEURON_INPUT_COUNT);
    public static final OutputLayer outLayer = new OutputLayer(INNER_NEURON_INPUT_COUNT, INNER_NEURON_COUNT);

    public void setInput(double x) {
        innerLayer.setInputs(new Double[]{x});
        outLayer.setInputs(innerLayer.getOutput());
    }

    public Double[] getOutput() {
        return outLayer.getOutput();
    }



}
