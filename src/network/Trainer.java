package network;

import java.util.List;

/**
 * Created by kucherenko on 18.04.15.
 */
public class Trainer {

    private final Network network;

    public Trainer(Network network) {
        this.network = network;
    }

    public Network getTrainedNetwork() {

        final double [] x = {0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0};
        final double [] targets = {0.000549,
                0.310758,
                0.450369,
                0.57186,
                0.681125,
                0.752747,
                0.815808,
                0.857848,
                0.893805,
                0.928823,
                0.969081};


        for (int i = 0; i < 10000; i++) {

            int index = (int)(Math.random() * 10);

            network.setInput(x[index]);

            network.outLayer.setTarget(targets[index]);
            network.outLayer.calculateDelta();
            network.outLayer.correctWeights();

            for(int j = 0; j < network.INNER_NEURON_COUNT; j++) {


                double previousDelta = network.outLayer.getDeltaOfNeuron(0);
                List<Double> weights = network.outLayer.getWeightsOfNeuron(0);
                double previousWeight = weights.get(1);

                network.innerLayer.calculateDeltaOfNeuron(j, previousDelta, previousWeight);

                network.innerLayer.correctWeights();

            }

        }
        return network;
    }
}
