package network;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kucherenko on 18.04.15.
 */
public class Neuron {

    private final String tag;

    private List<Double> inputs = new ArrayList<Double>() {
        {
            add(0, 1.0);
        }
    };

    private double delta;


    private List<Double> weights = new ArrayList<Double>();

    private double s;

    public interface ActivationFunction {
        public double function(double y);
    }

    public Neuron(int inputCount, String tag) {
        this.tag = tag;
        //  initWeights(++inputCount);
        initRandomWeights(++inputCount);
        /// System.out.println(tag + " init with count " + inputCount);
    }

    public double getDelta() {
        return delta;
    }

    public List<Double> getWeights() {
        return weights;
    }

    public void setDelta(double delta) {
        this.delta = delta;
    }

    private void initWeights(int weightsCount) {
        for (int i = 0; i < weightsCount; i++)
            weights.add(-100.0);
        for (int i = 0; i < weightsCount; i++)
            System.out.println(tag + " веса " + weights.get(i));

    }

    private void initRandomWeights(int weightsCount) {
        for (int i = 0; i < weightsCount; i++)
            weights.add(Math.random() - 0.5);
    }

    public void setInputs(Double[] inputs) {
        for (double x : inputs)
            this.inputs.add(x);
        calculateSum();
    }

    public double getOutput() {
        ActivationFunction activationFunction = new SigmoidFunction();
        return activationFunction.function(s);
    }

    private void calculateSum() {

        s = 0;

       /// System.out.println("summmmmmmmmmmmmmmmmmmmmmmm" + s);

        for (int i = 0; i < weights.size(); i++) {
          //  System.out.println(i + " ves  "+ weights.get(i));
          //  System.out.println(i + " vhod " + inputs.get(i));

            s += weights.get(i) * inputs.get(i);
        }

        // System.out.println(tag + " сумма " + s);
    }

    public void correctWeight() {
        for(int i = 0; i < weights.size(); i++) {
            weights.set(i, weights.get(i) + Constants.STEP * delta * inputs.get(i));

        }
    }

    public class SigmoidFunction implements ActivationFunction {

        public double function(double y) {
            return 1.0 / (1.0 + Math.exp(-Constants.ALPHA * y));
        }
    }

}
