package layer;

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import network.Neuron;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kucherenko on 18.04.15.
 */
public class Layer {

    protected List<Neuron> neuronList = new ArrayList<Neuron>();

    protected int neuronInputCount;

    protected List<Double> outputs = new ArrayList<Double>();

    public Layer(int neuronCount, int neuronInputCount) {
        this.neuronInputCount = neuronInputCount;
        while(neuronList.size() < neuronCount) {
            neuronList.add(new Neuron(neuronInputCount, "layer"));
        }
    }

    public void setInputs(Double [] inputs) {
        outputs.clear();
        if(inputs.length != neuronInputCount) throw new IllegalArgumentException();

        for(Neuron neuron: neuronList) {
            neuron.setInputs(inputs);
        }
    }

    public Double[] getOutput() {

        for(Neuron neuron: neuronList) {
            outputs.add(neuron.getOutput());
            System.out.println(neuron.getOutput());
        }

        Double [] outputsArray = new Double[outputs.size()];



        return outputs.toArray(outputsArray);
    }


}
