package layer;

import network.Neuron;

import java.util.List;

/**
 * Created by kucherenko on 18.04.15.
 */
public class OutputLayer extends Layer {

    private final double [] targets = {0.000549,
            0.310758,
            0.450369,
            0.57186,
            0.681125,
            0.752747,
            0.815808,
            0.857848,
            0.893805,
            0.928823,
            0.969081};

    private double target;

    public OutputLayer(int neuronCount, int neuronInputCount) {
        super(neuronCount, neuronInputCount);
    }

    public void setTarget(double target) {
        this.target = target;
    }

    public void calculateDelta() {
        for(int i = 0; i < outputs.size(); i++) {
            double delta = outputs.get(i) * (1.0 - outputs.get(i)) * (target - outputs.get(i));
            neuronList.get(i).setDelta(delta);
        }
    }

    public List<Double> getWeightsOfNeuron(int index) {
        return neuronList.get(index).getWeights();
    }

    public double getDeltaOfNeuron(int index) {
        return neuronList.get(index).getDelta();
    }

    public void correctWeights() {
        for(Neuron neuron: neuronList) {
            neuron.correctWeight();
        }
    }

}
