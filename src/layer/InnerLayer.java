package layer;

import network.Neuron;

/**
 * Created by kucherenko on 18.04.15.
 */
public class InnerLayer extends Layer {

    public InnerLayer(int neuronCount, int neuronInputCount) {
        super(neuronCount, neuronInputCount);
    }

    public void calculateDeltaOfNeuron(int index, double previousDelta, double previousWeight) {

            double delta = outputs.get(index) * (1.0 - outputs.get(index)) * previousDelta * previousWeight;
            neuronList.get(index).setDelta(delta);

    }

    public void correctWeights() {
        for(Neuron neuron: neuronList) {
            neuron.correctWeight();
        }
    }

}
